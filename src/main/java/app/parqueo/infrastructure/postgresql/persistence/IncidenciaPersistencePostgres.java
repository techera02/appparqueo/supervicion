package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Incidencia;
import app.parqueo.domain.persistence_ports.IncidenciaPersistence;
import app.parqueo.infrastructure.repositories.IncidenciaRepository;


@Repository("IncidenciaPersistence")
public class IncidenciaPersistencePostgres implements IncidenciaPersistence
{
	private final IncidenciaRepository incidenciaRepository;
	
    @Autowired
    public IncidenciaPersistencePostgres(IncidenciaRepository incidenciaRepository) 
    {
        this.incidenciaRepository = incidenciaRepository;
    }
    

	@Override
	public Integer insertarIncidencia(Incidencia incidencia) 
	{
		return this.incidenciaRepository.insertarIncidencia(incidencia.getTinc_Id(), incidencia.getSupe_Id(),
				incidencia.getClie_Id(), incidencia.getInci_Descripcion(), incidencia.getInci_Placa());
	}

	
	
}