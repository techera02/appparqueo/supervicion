package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Supervisor;
import app.parqueo.domain.persistence_ports.SupervisorPersistence;

import app.parqueo.infrastructure.repositories.SupervisorRepository;




@Repository("SupervisorPersistence")
public class SupervisorPersistencePostgres implements SupervisorPersistence {

	private final SupervisorRepository supervisorRepository;
	
    @Autowired
    public SupervisorPersistencePostgres(SupervisorRepository supervisorRepository) 
    {
        this.supervisorRepository = supervisorRepository;
    }
	
	@Override
	public Integer verificarSupervisor(String usuario, String clave) 
	{
		return this.supervisorRepository.verificarSupervisor(usuario, clave);
	}
	

	@Override
	public Supervisor seleccionarSupervisor(Integer idSupervisor) {
		return this.supervisorRepository.seleccionarSupervisor(idSupervisor).toSupervisor();
	}


	@Override
	public Integer validarSupervisorExiste(Integer idSupervisor) {
		return this.supervisorRepository.validarSupervisorExiste(idSupervisor);
	}
}
