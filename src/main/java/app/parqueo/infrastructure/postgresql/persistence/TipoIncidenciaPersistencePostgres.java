package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.TipoIncidencia;
import app.parqueo.domain.persistence_ports.TipoIncidenciaPersistence;
import app.parqueo.infrastructure.postgresql.entities.TipoIncidenciaEntity;
import app.parqueo.infrastructure.repositories.TipoIncidenciaRepository;

@Repository("TipoIncidenciaPersistence")
public class TipoIncidenciaPersistencePostgres implements TipoIncidenciaPersistence{
	
	private final TipoIncidenciaRepository tipoIncidenciaRepository;
	
    @Autowired
    public TipoIncidenciaPersistencePostgres(TipoIncidenciaRepository tipoIncidenciaRepository) 
    {
        this.tipoIncidenciaRepository = tipoIncidenciaRepository;
    }
    
	@Override
	public List<TipoIncidencia> listarTipoIncidencias() {
		List<TipoIncidencia> lista = new ArrayList<TipoIncidencia>();
		List<TipoIncidenciaEntity> listaEntity = this.tipoIncidenciaRepository.listarTipoIncidencias();
		
		for(int x=0; x<listaEntity.size();x++) {
			TipoIncidencia tipoIncidencia = listaEntity.get(x).toTipoIncidencia();
			lista.add(tipoIncidencia);
		}
		return lista;
	}

}
