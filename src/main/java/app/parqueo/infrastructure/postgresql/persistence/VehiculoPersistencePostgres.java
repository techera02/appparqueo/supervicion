package app.parqueo.infrastructure.postgresql.persistence;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.Vehiculo;
import app.parqueo.domain.persistence_ports.VehiculoPersistence;
import app.parqueo.infrastructure.postgresql.entities.VehiculoEntity;
import app.parqueo.infrastructure.repositories.VehiculoRepository;

@Repository("VehiculoPersistence")
public class VehiculoPersistencePostgres implements VehiculoPersistence {

	private final VehiculoRepository vehiculoRepository;
	
	@Autowired
    public VehiculoPersistencePostgres(VehiculoRepository vehiculoRepository) 
    {
        this.vehiculoRepository = vehiculoRepository;
    }
	
	
	@Override
	public List<Vehiculo> listarVehiculo(String placa) {
		List<VehiculoEntity> listaVehiculoEntity = this.vehiculoRepository.listarVehiculo(placa);
		List<Vehiculo> listaVehiculo = new ArrayList<Vehiculo>();
		
		for(int x = 0;x<listaVehiculoEntity.size();x++) {
			Vehiculo entity = new Vehiculo();
			entity.setVehi_Placa(listaVehiculoEntity.get(x).getVehi_Placa());
			entity.setVehi_Id(listaVehiculoEntity.get(x).getVehi_Id());
			entity.setClie_Id(listaVehiculoEntity.get(x).getClie_Id());
			entity.setVehi_Eliminado(listaVehiculoEntity.get(x).getVehi_Eliminado());
			entity.setVehi_FechaCreacion(listaVehiculoEntity.get(x).getVehi_FechaCreacion());
			entity.setVehi_FechaElimina(listaVehiculoEntity.get(x).getVehi_FechaElimina());

			listaVehiculo.add(entity);
		}
		return listaVehiculo;
	}

}


