package app.parqueo.infrastructure.postgresql.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.persistence_ports.IncidenciaFotoPersistence;
import app.parqueo.infrastructure.repositories.IncidenciaFotoRepository;




@Repository("IncidenciaFotoPersistence")
public class IncidenciaFotoPersistencePostgres implements IncidenciaFotoPersistence
{
	private final IncidenciaFotoRepository incidenciaFotoRepository;
	
    @Autowired
    public IncidenciaFotoPersistencePostgres(IncidenciaFotoRepository incidenciaFotoRepository) 
    {
        this.incidenciaFotoRepository = incidenciaFotoRepository;
    }
    

	@Override
	public Integer insertarIncidenciaFoto(IncidenciaFoto incidenciaFoto) 
	{
		return this.incidenciaFotoRepository.insertarIncidenciaFoto(incidenciaFoto.getInci_Id(), incidenciaFoto.getIfot_NombreArchivo());
	}


	
	
}

