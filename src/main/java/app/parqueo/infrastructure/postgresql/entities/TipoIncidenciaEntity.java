package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import app.parqueo.domain.model.TipoIncidencia;

@Entity
@Table(name="\"TipoIncidencia\"")
public class TipoIncidenciaEntity {
	
	@Id
	private Integer Tinc_Id;
	
	private String Tinc_Nombre;
	private boolean Tinc_Eliminado;
	private Integer Tinc_UsuarioCreacion;
	private Integer Tinc_UsuarioEdicion;
	private Integer Tinc_UsuarioElimina;
	private Timestamp Tinc_FechaCreacion;
	private Timestamp Tinc_FechaEdicion;
	private Timestamp Tinc_FechaElimina;
	
	public Integer getTinc_Id() {
		return Tinc_Id;
	}
	public void setTinc_Id(Integer tinc_Id) {
		Tinc_Id = tinc_Id;
	}
	public String getTinc_Nombre() {
		return Tinc_Nombre;
	}
	public void setTinc_Nombre(String tinc_Nombre) {
		Tinc_Nombre = tinc_Nombre;
	}
	public boolean isTinc_Eliminado() {
		return Tinc_Eliminado;
	}
	public void setTinc_Eliminado(boolean tinc_Eliminado) {
		Tinc_Eliminado = tinc_Eliminado;
	}
	public Integer getTinc_UsuarioCreacion() {
		return Tinc_UsuarioCreacion;
	}
	public void setTinc_UsuarioCreacion(Integer tinc_UsuarioCreacion) {
		Tinc_UsuarioCreacion = tinc_UsuarioCreacion;
	}
	public Integer getTinc_UsuarioEdicion() {
		return Tinc_UsuarioEdicion;
	}
	public void setTinc_UsuarioEdicion(Integer tinc_UsuarioEdicion) {
		Tinc_UsuarioEdicion = tinc_UsuarioEdicion;
	}
	public Integer getTinc_UsuarioElimina() {
		return Tinc_UsuarioElimina;
	}
	public void setTinc_UsuarioElimina(Integer tinc_UsuarioElimina) {
		Tinc_UsuarioElimina = tinc_UsuarioElimina;
	}
	public Timestamp getTinc_FechaCreacion() {
		return Tinc_FechaCreacion;
	}
	public void setTinc_FechaCreacion(Timestamp tinc_FechaCreacion) {
		Tinc_FechaCreacion = tinc_FechaCreacion;
	}
	public Timestamp getTinc_FechaEdicion() {
		return Tinc_FechaEdicion;
	}
	public void setTinc_FechaEdicion(Timestamp tinc_FechaEdicion) {
		Tinc_FechaEdicion = tinc_FechaEdicion;
	}
	public Timestamp getTinc_FechaElimina() {
		return Tinc_FechaElimina;
	}
	public void setTinc_FechaElimina(Timestamp tinc_FechaElimina) {
		Tinc_FechaElimina = tinc_FechaElimina;
	}
	
	public TipoIncidencia toTipoIncidencia() {
		TipoIncidencia tipoIncidencia = new TipoIncidencia();
        BeanUtils.copyProperties(this, tipoIncidencia);
        return tipoIncidencia;
    }
}
