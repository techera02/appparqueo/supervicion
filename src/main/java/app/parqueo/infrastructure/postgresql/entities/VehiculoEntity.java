package app.parqueo.infrastructure.postgresql.entities;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="\"Vehiculo\"")
public class VehiculoEntity {

	@Id
	private Integer Vehi_Id;
	private Integer Clie_Id;
	private String Vehi_Placa;
	private boolean Vehi_Eliminado;
	private Timestamp Vehi_FechaCreacion;
	private Timestamp Vehi_FechaElimina;

	
	public int getVehi_Id() 
	{
		return Vehi_Id;
	}
	public void setVehi_Id(int vehi_Id) 
	{
		Vehi_Id = vehi_Id;
	}
	
	public int getClie_Id() 
	{
		return Clie_Id;
	}
	public void setClie_Id(int clie_Id) 
	{
		Clie_Id = clie_Id;
	}
	
	public String getVehi_Placa() 
	{
		return Vehi_Placa;
	}
	public void setVehi_Id(String vehi_Placa) 
	{
		Vehi_Placa = vehi_Placa;
	}
	
	public boolean getVehi_Eliminado() 
	{
		return Vehi_Eliminado;
	}
	public void setVehi_Eliminado(boolean vehi_Eliminado) 
	{
		Vehi_Eliminado = vehi_Eliminado;
	}
	
	public Timestamp getVehi_FechaCreacion() 
	{
		return Vehi_FechaCreacion;
	}
	public void setVehi_FechaCreacion(Timestamp vehi_FechaCreacion) 
	{
		Vehi_FechaCreacion = vehi_FechaCreacion;
	}
	
	public Timestamp getVehi_FechaElimina() 
	{
		return Vehi_FechaElimina;
	}
	public void setVehi_FechaElimina(Timestamp vehi_FechaElimina) 
	{
		Vehi_FechaElimina = vehi_FechaElimina;
	}
	
}
