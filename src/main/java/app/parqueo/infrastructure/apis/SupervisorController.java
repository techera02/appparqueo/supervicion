package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.SupervisorService;
import app.parqueo.domain.model.ResponseSeleccionarSupervisor;
import app.parqueo.domain.model.ResponseIniciarSesionSupervisor;
import app.parqueo.domain.model.Supervisor;

@RestController
@RequestMapping("/Supervisor")
public class SupervisorController {

	private final SupervisorService supervisorService;

	@Autowired
	public SupervisorController(SupervisorService supervisorService) {
		this.supervisorService = supervisorService;
	}

	@PostMapping("/IniciarSesionSupervisor")
	public ResponseEntity<ResponseIniciarSesionSupervisor> iniciarSesionSupervisor(@RequestBody Supervisor supervisor) {
		ResponseIniciarSesionSupervisor response = new ResponseIniciarSesionSupervisor();

		try {
			Integer temp = supervisorService.verificarSupervisor(supervisor.getSupe_Alias(), supervisor.getSupe_Clave());

			if (temp > 0) {
				response.setEstado(true);
				response.setSupe_Id(temp);
			} else {
				response.setCodError(1);
				response.setMensaje("El usuario o clave son incorrectos.");
				response.setEstado(false);				
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping("/SeleccionarSupervisor/{idSupervisor}")
	public ResponseEntity<ResponseSeleccionarSupervisor> seleccionarSupervisor(
			@PathVariable("idSupervisor") Integer idSupervisor) {
		ResponseSeleccionarSupervisor response = new ResponseSeleccionarSupervisor();
		response.setEstado(true);

		try {
			Integer temp = this.supervisorService.validarSupervisorExiste(idSupervisor);

			if (temp == 1) {
				response.setSupervisor(this.supervisorService.seleccionarSupervisor(idSupervisor));
			} else {
				response.setCodError(1);
				response.setMensaje("Supervisor no registrado");
				response.setEstado(false);
			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}
		return ResponseEntity.ok(response);
	}

}
