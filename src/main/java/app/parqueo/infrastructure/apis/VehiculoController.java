package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.VehiculoService;
import app.parqueo.domain.model.ResponseListarVehiculos;


@RestController
@RequestMapping("/Vehiculo")
public class VehiculoController {

	private final VehiculoService vehiculoService;
	
	@Autowired
    public VehiculoController(VehiculoService vehiculoService) 
	{
        this.vehiculoService = vehiculoService;
    }
	
	
	
	@GetMapping("/ConsultarPlaca/{placa}")
    public ResponseEntity<ResponseListarVehiculos> listarVehiculo
    (@PathVariable("placa")String placa)
	{  
		ResponseListarVehiculos response = new ResponseListarVehiculos();
		
			try {
				
				response.setLista(vehiculoService.listarVehiculo(placa));
				
				
			}catch(Exception e) {
				response.setCodError(0);
				response.setEstado(false);
				response.setMensaje(e.getMessage());
			}
		
		return ResponseEntity.ok(response);
    }
	
}
