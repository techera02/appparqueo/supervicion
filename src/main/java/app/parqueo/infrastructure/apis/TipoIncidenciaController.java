package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.TipoIncidenciaService;
import app.parqueo.domain.model.ResponseListarTipoIncidencias;

@RestController
@RequestMapping("/TipoIncidencia")
public class TipoIncidenciaController {
	
	private final TipoIncidenciaService tipoIncidenciaService;
	
	@Autowired
    public TipoIncidenciaController(TipoIncidenciaService tipoIncidenciaService) 
	{
        this.tipoIncidenciaService = tipoIncidenciaService;
    }
	
	@GetMapping("/ListarTipoIncidencias")
    public ResponseEntity<ResponseListarTipoIncidencias> listarVehiculo()
	{  
		ResponseListarTipoIncidencias response = new ResponseListarTipoIncidencias();
		response.setEstado(true);

		try {
			response.setLista(tipoIncidenciaService.listarTipoIncidencias());	
		}catch(Exception e) {
			response.setCodError(0);
			response.setEstado(false);
			response.setMensaje(e.getMessage());
		}
		
		return ResponseEntity.ok(response);
    }
}
