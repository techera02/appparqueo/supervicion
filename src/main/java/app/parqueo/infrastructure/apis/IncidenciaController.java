package app.parqueo.infrastructure.apis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.parqueo.application.IncidenciaFotoService;
import app.parqueo.application.IncidenciaService;
import app.parqueo.domain.model.Incidencia;
import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.model.RequestIncidencia;
import app.parqueo.domain.model.ResponseInsertarIncidencia;

@RestController
@RequestMapping("/Incidencia")
public class IncidenciaController {

	private final IncidenciaService incidenciaService;
	private final IncidenciaFotoService incidenciaFotoService;

	@Autowired
	public IncidenciaController(IncidenciaService incidenciaService, IncidenciaFotoService incidenciaFotoService) {
		this.incidenciaService = incidenciaService;
		this.incidenciaFotoService = incidenciaFotoService;
	}

	@PostMapping("/InsertarIncidencia")
	public ResponseEntity<ResponseInsertarIncidencia> insertarIncidencia(@RequestBody RequestIncidencia request) {

		ResponseInsertarIncidencia response = new ResponseInsertarIncidencia();

		try {

			Incidencia incidencia = new Incidencia();
			incidencia.setTinc_Id(request.getTinc_Id());
			incidencia.setSupe_Id(request.getSupe_Id());
			incidencia.setClie_Id(request.getClie_Id());
			incidencia.setInci_Descripcion(request.getInci_Descripcion());
			incidencia.setInci_Placa(request.getInci_Placa());

			Integer indice = this.incidenciaService.insertarIncidencia(incidencia);

			if (indice > 0) {

				for (int x = 0; x < request.getFotos().size(); x++) {

					IncidenciaFoto incidenciaFoto = new IncidenciaFoto();

					incidenciaFoto.setInci_Id(indice);
					incidenciaFoto.setIfot_NombreArchivo(request.getFotos().get(x).getIfot_NombreArchivo());

					this.incidenciaFotoService.insertarIncidenciaFoto(incidenciaFoto);
				}

				response.setMensaje("Ok");
				response.setEstado(true);
				response.setInci_Id(indice);

			}

		} catch (Exception e) {
			response.setCodError(0);
			response.setMensaje("Error no controlado");
			response.setEstado(false);
		}

		return ResponseEntity.ok(response);
	}

}
