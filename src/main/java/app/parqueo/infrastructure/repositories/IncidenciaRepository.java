package app.parqueo.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.IncidenciaEntity;


public interface IncidenciaRepository  extends JpaRepository<IncidenciaEntity, Integer>{
	
	
	
	@Query(value = "SELECT \"UFN_InsertarIncidencia\"(:tinc_id,:supe_id,:clie_id,:inci_descripcion,:inci_placa)", nativeQuery = true)
	Integer insertarIncidencia(
			@Param("tinc_id") Integer tinc_id, @Param("supe_id") Integer supe_id,
			@Param("clie_id") Integer clie_id,
			@Param("inci_descripcion") String inci_descripcion, @Param("inci_placa") String inci_placa
			);

}
