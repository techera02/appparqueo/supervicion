package app.parqueo.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import app.parqueo.infrastructure.postgresql.entities.TipoIncidenciaEntity;

public interface TipoIncidenciaRepository  extends JpaRepository<TipoIncidenciaEntity, Integer>{
	@Query(value = "SELECT * FROM \"UFN_ListarTipoIncidencias\"()", nativeQuery = true)
	List<TipoIncidenciaEntity> listarTipoIncidencias();
}
