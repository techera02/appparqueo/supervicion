package app.parqueo.infrastructure.repositories;


import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.SupervisorEntity;


public interface SupervisorRepository  extends JpaRepository<SupervisorEntity, Integer>
{
	@Query(value = "SELECT \"UFN_ValidarLoginSupervisor\"(:usuario,:clave)", nativeQuery = true)
	Integer verificarSupervisor(@Param("usuario") String usuario, 
								@Param("clave") String clave);
	
	@Query(value = "SELECT * FROM \"UFN_SeleccionarSupervisor\"(:idSupervisor)", nativeQuery = true)
	SupervisorEntity seleccionarSupervisor(@Param("idSupervisor") Integer idSupervisor);
	
	@Query(value = "SELECT * FROM \"UFN_ValidarSupervisorExiste\"(:idSupervisor)", nativeQuery = true)
	Integer validarSupervisorExiste(@Param("idSupervisor") Integer idSupervisor);
}
