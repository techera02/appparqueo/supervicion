package app.parqueo.infrastructure.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.IncidenciaFotoEntity;




public interface IncidenciaFotoRepository  extends JpaRepository<IncidenciaFotoEntity, Integer>{
	
	@Query(value = "SELECT \"UFN_InsertarIncidenciaFoto\"(:inci_id,:ifot_nombrearchivo)", nativeQuery = true)
	Integer insertarIncidenciaFoto(
			@Param("inci_id") Integer inci_id, @Param("ifot_nombrearchivo") String ifot_nombrearchivo
			);
	
}
