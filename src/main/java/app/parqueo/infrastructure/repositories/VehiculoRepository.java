package app.parqueo.infrastructure.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import app.parqueo.infrastructure.postgresql.entities.VehiculoEntity;

public interface VehiculoRepository  extends JpaRepository<VehiculoEntity, Integer> {
	@Query(value = "SELECT * FROM \"UFN_ConsultarPlaca\"(:placa)", nativeQuery = true)
	List<VehiculoEntity> listarVehiculo(@Param("placa") String placa);
}


