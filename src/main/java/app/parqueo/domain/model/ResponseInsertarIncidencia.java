package app.parqueo.domain.model;

public class ResponseInsertarIncidencia extends ResponseError {

	private Integer Inci_Id;

	public Integer getInci_Id() {
		return Inci_Id;
	}

	public void setInci_Id(Integer inci_Id) {
		Inci_Id = inci_Id;
	}

}
