package app.parqueo.domain.model;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Supervisor {
	@JsonProperty
	private int supe_Id;
	@JsonProperty
	private String supe_Nombres;
	@JsonProperty
	private String supe_ApellidoPaterno;
	@JsonProperty
	private String supe_ApellidoMaterno;
	@JsonProperty
	private int tdoc_Id;
	@JsonProperty
	private String supe_NroDocumento;
	@JsonProperty
	private String supe_TokenFirebase;
	@JsonProperty
	private Boolean supe_Activo;
	@JsonProperty
	private int supe_UsuarioCreacion;
	@JsonProperty
	private int supe_UsuarioEdicion;
	@JsonProperty
	private Timestamp supe_FechaCreacion;
	@JsonProperty
	private Timestamp supe_FechaEdicion;
	@JsonProperty
	private String supe_Correo;	
	@JsonProperty
	private String supe_Alias;
	@JsonProperty
	private String supe_Clave;

	
	public Supervisor() {
		
	}
	
	public int getSupe_Id() 
	{
		return supe_Id;
	}
	public void setSupe_Id(int supe_Id) 
	{
		this.supe_Id = supe_Id;
	}
	
	public String getSupe_Nombres() 
	{
		return supe_Nombres;
	}
	public void setSupe_Nombres(String supe_Nombres) 
	{
		this.supe_Nombres = supe_Nombres;
	}
	
	public String getSupe_ApellidoPaterno() 
	{
		return supe_ApellidoPaterno;
	}
	public void setSupe_ApellidoPaterno(String supe_ApellidoPaterno) 
	{
		this.supe_ApellidoPaterno = supe_ApellidoPaterno;
	}
	
	public String getSupe_ApellidoMaterno() 
	{
		return supe_ApellidoMaterno;
	}
	public void setSupe_ApellidoMaterno(String supe_ApellidoMaterno) 
	{
		this.supe_ApellidoMaterno = supe_ApellidoMaterno;
	}
	
	public int getTdoc_Id() 
	{
		return tdoc_Id;
	}
	public void setTdoc_Id(int tdoc_Id) 
	{
		this.tdoc_Id = tdoc_Id;
	}
	
	public String getSupe_NroDocumento() 
	{
		return supe_NroDocumento;
	}
	public void setSupe_NroDocumento(String supe_NroDocumento) 
	{
		this.supe_NroDocumento = supe_NroDocumento;
	}
	
	public String getSupe_TokenFirebase() 
	{
		return supe_TokenFirebase;
	}
	public void setSupe_TokenFirebase(String supe_TokenFirebase) 
	{
		this.supe_TokenFirebase = supe_TokenFirebase;
	}
	
	public Boolean getSupe_Activo() 
	{
		return supe_Activo;
	}
	public void setSupe_Activo(Boolean supe_Activo) 
	{
		this.supe_Activo = supe_Activo;
	}
	
	public int getSupe_UsuarioCreacion() 
	{
		return supe_UsuarioCreacion;
	}
	public void setSupe_UsuarioCreacion(int supe_UsuarioCreacion) 
	{
		this.supe_UsuarioCreacion = supe_UsuarioCreacion;
	}
	
	public int getSupe_UsuarioEdicion() 
	{
		return supe_UsuarioEdicion;
	}
	public void setSupe_UsuarioEdicion(int supe_UsuarioEdicion) 
	{
		this.supe_UsuarioEdicion = supe_UsuarioEdicion;
	}
	
	public Timestamp getSupe_FechaCreacion() 
	{
		return supe_FechaCreacion;
	}
	public void setSupe_FechaCreacion(Timestamp supe_FechaCreacion) 
	{
		this.supe_FechaCreacion = supe_FechaCreacion;
	}
	
	public Timestamp getSupe_FechaEdicion() 
	{
		return supe_FechaEdicion;
	}
	public void setSupe_FechaEdicion(Timestamp supe_FechaEdicion) 
	{
		this.supe_FechaEdicion = supe_FechaEdicion;
	}
	
	public String getSupe_Correo() 
	{
		return supe_Correo;
	}
	public void setSupe_Correo(String supe_Correo) 
	{
		this.supe_Correo = supe_Correo;
	}
	
	public String getSupe_Alias() {
		return supe_Alias;
	}
	public void setSupe_Alias(String supe_Alias) {
		this.supe_Alias = supe_Alias;
	}
	
	public String getSupe_Clave() {
		return supe_Clave;
	}

	public void setSupe_Clave(String supe_Clave) {
		this.supe_Clave = supe_Clave;
	}
}
