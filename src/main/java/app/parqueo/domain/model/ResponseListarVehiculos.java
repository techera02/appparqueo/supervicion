package app.parqueo.domain.model;
import java.util.List;

public class ResponseListarVehiculos extends ResponseError{
		
		private List<Vehiculo> lista;
		
		public List<Vehiculo> getLista() {
			return lista;
		}

		public void setLista(List<Vehiculo> lista) {
			this.lista = lista;
		}
				
		
}
