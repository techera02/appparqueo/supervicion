package app.parqueo.domain.model;

public class ResponseIniciarSesionSupervisor extends ResponseError {

	private Integer Supe_Id;

	public Integer getSupe_Id() {
		return Supe_Id;
	}

	public void setSupe_Id(Integer supe_Id) {
		Supe_Id = supe_Id;
	}
}
