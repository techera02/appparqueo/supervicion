package app.parqueo.domain.model;

public class Incidencia 
{
	private Integer tinc_Id;
	private Integer supe_Id;
	private Integer clie_Id;
	private String inci_Descripcion;
	private String inci_Placa;
	private String ifot_NombreArchivo;

	public Incidencia() {
		
	}
	
	public Integer getTinc_Id() {
		return tinc_Id;
	}
	public void setTinc_Id(Integer tinc_id) {
		this.tinc_Id = tinc_id;
	}

	public Integer getSupe_Id() {
		return supe_Id;
	}
	public void setSupe_Id(Integer supe_id) {
		this.supe_Id = supe_id;
	}

	public Integer getClie_Id() {
		return clie_Id;
	}
	public void setClie_Id(Integer clie_id) {
		this.clie_Id = clie_id;
	}

	public String getInci_Descripcion() {
		return inci_Descripcion;
	}
	public void setInci_Descripcion(String inci_descripcion) {
		this.inci_Descripcion = inci_descripcion;
	}

	public String getInci_Placa() {
		return inci_Placa;
	}
	public void setInci_Placa(String inci_placa) {
		this.inci_Placa = inci_placa;
	}

	public String getIfot_NombreArchivo() {
		return ifot_NombreArchivo;
	}
	public void setIfot_NombreArchivo(String ifot_nombrearchivo) {
		this.ifot_NombreArchivo = ifot_nombrearchivo;
	}
	
}

