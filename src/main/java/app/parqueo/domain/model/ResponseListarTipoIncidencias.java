package app.parqueo.domain.model;

import java.util.List;

public class ResponseListarTipoIncidencias extends ResponseError {
	private List<TipoIncidencia> lista;

	public List<TipoIncidencia> getLista() {
		return lista;
	}

	public void setLista(List<TipoIncidencia> lista) {
		this.lista = lista;
	}
	
	
}
