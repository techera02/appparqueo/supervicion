package app.parqueo.domain.model;

public class IncidenciaFoto {

	private Integer inci_Id;
	private String ifot_NombreArchivo;



	public IncidenciaFoto() {
		
	}
	
	public Integer getInci_Id() {
		return inci_Id;
	}
	public void setInci_Id(Integer inci_id) {
		this.inci_Id = inci_id;
	}
	
	public String getIfot_NombreArchivo() {
		return ifot_NombreArchivo;
	}
	public void setIfot_NombreArchivo(String ifot_nombrearchivo) {
		this.ifot_NombreArchivo = ifot_nombrearchivo;
	}
}

