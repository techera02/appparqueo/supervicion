package app.parqueo.domain.model;

public class ResponseSeleccionarSupervisor {
	private boolean estado;
	private String mensaje;
	private Integer codError;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public Integer getCodError() {
		return codError;
	}
	public void setCodError(Integer codError) {
		this.codError = codError;
	}
	private Supervisor supervisor;
	
	public boolean getEstado() 
	{
		return estado;
	}
	public void setEstado(boolean estado) 
	{
		this.estado = estado;
	}
	public Supervisor getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(Supervisor supervisor) {
		this.supervisor = supervisor;
	}
	



}
