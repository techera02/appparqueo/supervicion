package app.parqueo.domain.model;

import java.util.List;

import app.parqueo.infrastructure.postgresql.entities.VehiculoEntity;

public class ResponseConsultarPlaca extends ResponseError{
	
	private String Vehi_Placa;
	private boolean estado;
	private String mensaje;
	private Integer codError;
	
	
	public String getVehi_Placa() {
		return Vehi_Placa;
	}

	public void setVehi_Placa(String vehi_Placa) {
		Vehi_Placa = vehi_Placa;
	}
	private List<VehiculoEntity> lista;

	public List<VehiculoEntity> getLista() {
		return lista;
	}

	public void setLista(List<VehiculoEntity> lista) {
		this.lista = lista;
	}
	public boolean getEstado() 
	{
		return estado;
	}
	public void setEstado(boolean estado) 
	{
		this.estado = estado;
	}
	public String getMensaje() 
	{
		return mensaje;
	}
	public void setMensaje(String mensaje) 
	{
		this.mensaje = mensaje;
	}
	public Integer getCodError() 
	{
		return codError;
	}
	public void setCodError(Integer codError) 
	{
		this.codError = codError;
	}
}
