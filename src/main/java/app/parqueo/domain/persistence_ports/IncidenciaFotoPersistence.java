package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.IncidenciaFoto;

public interface IncidenciaFotoPersistence {

	Integer insertarIncidenciaFoto(IncidenciaFoto incidenciaFoto);
}
