package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.TipoIncidencia;

public interface TipoIncidenciaPersistence {
	List<TipoIncidencia> listarTipoIncidencias();
}
