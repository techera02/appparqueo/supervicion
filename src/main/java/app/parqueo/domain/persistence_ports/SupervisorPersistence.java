package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.Supervisor;


public interface SupervisorPersistence {
	
	Integer verificarSupervisor(String usuario, String clave);
	
	Supervisor seleccionarSupervisor(Integer idSupervisor);
	
	
	Integer validarSupervisorExiste(Integer idCliente);
}
