package app.parqueo.domain.persistence_ports;

import app.parqueo.domain.model.Incidencia;


public interface IncidenciaPersistence 
{
	Integer insertarIncidencia(Incidencia incidencia);

}
