package app.parqueo.domain.persistence_ports;

import java.util.List;

import app.parqueo.domain.model.Vehiculo;

public interface VehiculoPersistence {

	List<Vehiculo> listarVehiculo(String placa);

}