package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.TipoIncidencia;
import app.parqueo.domain.persistence_ports.TipoIncidenciaPersistence;

@Service
public class TipoIncidenciaService {

	TipoIncidenciaPersistence tipoIncidenciaPersistence;
	
	public TipoIncidenciaService(TipoIncidenciaPersistence tipoIncidenciaPersistence)
	{
		this.tipoIncidenciaPersistence = tipoIncidenciaPersistence;
	}
	
	public List<TipoIncidencia> listarTipoIncidencias(){
		return this.tipoIncidenciaPersistence.listarTipoIncidencias();
	}
}
