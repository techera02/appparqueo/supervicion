package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Supervisor;
import app.parqueo.domain.persistence_ports.SupervisorPersistence;


@Service
public class SupervisorService {
	
	SupervisorPersistence supervisorPersistence;
	
	public SupervisorService(SupervisorPersistence supervisorPersistence)
	{
		this.supervisorPersistence = supervisorPersistence;
	}
	
	public Integer verificarSupervisor(String usuario, String clave) 
	{
        return this.supervisorPersistence.verificarSupervisor(usuario,clave);
    }
	
	public Supervisor seleccionarSupervisor(Integer idSupervisor) {
		return this.supervisorPersistence.seleccionarSupervisor(idSupervisor);
	}
	
	public Integer validarSupervisorExiste(Integer idCliente) {
		return this.supervisorPersistence.validarSupervisorExiste(idCliente);
	}
}
