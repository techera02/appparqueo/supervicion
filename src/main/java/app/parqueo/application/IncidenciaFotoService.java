package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.IncidenciaFoto;
import app.parqueo.domain.persistence_ports.IncidenciaFotoPersistence;




@Service
public class  IncidenciaFotoService {


	IncidenciaFotoPersistence incidenciaFotoPersistence;
	
	public IncidenciaFotoService(IncidenciaFotoPersistence incidenciaFotoPersistence)
	{
		this.incidenciaFotoPersistence = incidenciaFotoPersistence;
	}
	
	public Integer insertarIncidenciaFoto(IncidenciaFoto incidenciaFoto) 
	{
        return this.incidenciaFotoPersistence.insertarIncidenciaFoto(incidenciaFoto);
    }
	
	

}




