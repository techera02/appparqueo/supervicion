package app.parqueo.application;

import java.util.List;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Vehiculo;
import app.parqueo.domain.persistence_ports.VehiculoPersistence;






@Service
public class VehiculoService {
	
	VehiculoPersistence vehiculoPersistence;
	
	public VehiculoService(VehiculoPersistence vehiculoPersistence)
	{
		this.vehiculoPersistence = vehiculoPersistence;
	}
	
	public List<Vehiculo> listarVehiculo(String placa) 
	{
        return this.vehiculoPersistence.listarVehiculo(placa);
    }
}


