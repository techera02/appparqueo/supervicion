package app.parqueo.application;

import org.springframework.stereotype.Service;

import app.parqueo.domain.model.Incidencia;
import app.parqueo.domain.persistence_ports.IncidenciaPersistence;



@Service
public class  IncidenciaService {

	
	IncidenciaPersistence incidenciaPersistence;
	
	public IncidenciaService(IncidenciaPersistence incidenciaPersistence)
	{
		this.incidenciaPersistence = incidenciaPersistence;
	}
	
	public Integer insertarIncidencia(Incidencia incidencia) 
	{
        return this.incidenciaPersistence.insertarIncidencia(incidencia);
    }
}
